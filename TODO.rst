====
TODO
====

* Evaluate patches in ``doc/_static/theme_overrides.css`` once ``sphinx_rtd_theme``
  releases patches for Sphinx 2.x.
  See https://github.com/readthedocs/sphinx_rtd_theme/issues/741
  and https://github.com/readthedocs/sphinx_rtd_theme/pull/784
