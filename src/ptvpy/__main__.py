"""Entry point of the command line interface."""


import sys

from ._cli_root import main


if __name__ == "__main__":
    sys.exit(main())
