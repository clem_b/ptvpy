# Profile file for PtvPy
# The configuration below controls how PtvPy processes data.
#
# This file uses the markup language TOML 0.5.
# You can find an introduction to the syntax here:
# https://github.com/toml-lang/toml/blob/master/versions/en/toml-v0.5.0.md


[general]

# A glob pattern matching the image files to process. Match patterns similar to the
# Unix shell are used. https://docs.python.org/3/library/glob.html
# If the patten includes a relative path it is interpreted relative to the
# current working directory and not relative to the location of this file! Use
# an absolute path if you want to use this profile outside the current working
# directory.
# (str, required)
# default: "*.tif*"
data_files = "*.tif*"

# Use only a subset of all data files by specifing the index of the file to
# "start" with, the "step" between selected files (e.g. 2 will skip every other
# file) and / or the index of the file to "stop" with. These arguments behave
# exactly as the one's to Python's slice class.
# (integer, optional)
# subset.start =
# subset.step =
# subset.stop =

# Chose where PtvPy stores processed data. If the path is relative it is
# interpreted relative to the current working directory and not relative to the
# location of this file! Use an absolute path if you want to use this profile
# outside the current working directory.
# (string, required, default: "./ptvpy.h5")
storage_file = "./ptvpy.h5"

# If nothing else is specified through the command line the
# (array of "locate", "link", diff", required,
# default: ["locate", "link", "diff"])
default_steps = ["locate", "link", "diff"]


[step_locate]
# This table (section) contains parameters for the location of particles.

# The particle shape to detect (may be overwritten through the command line).
# Options prefixed with "helix" are ignored if the same value is not given
# here. Options not prefixed with "blob" or "helix" are always valid.
# ("blob" / "helix", required, default: "blob")
particle_shape = "blob"

# Remove average from each frame before particle location.
# (boolean, required, default: true)
remove_background = true

# This may be a single number or an array giving the feature's extent in each
# dimension, useful when the dimensions do not have equal resolution (e.g.
# confocal microscopy). The array order is the same as the image shape,
# conventionally (z, y, x) or (y, x). The number(s) must be odd integers.
# When in doubt, round up.
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.locate.html
# (positive float or 2-element array thereof, required, default: 11)
trackpy_locate.diameter = 11

# The minimum integrated brightness. This is a crucial parameter for
# eliminating spurious features. Recommended minimum values are 100 for
# integer images and 1 for float images.
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.locate.html
# (positive integer, required, default: 0)
trackpy_locate.minmass = 0

# Maximum radius-of-gyration of brightness.
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.locate.html
# (positive float, optional)
# trackpy_locate.maxsize =

# Minimum separtion between features. Default is diameter + 1. May be an array,
# see "diameter" for details. If not provided its value is derived as
# "diameter" + 1.
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.locate.html
# (positive float or 2-element array thereof, optional)
# trackpy_locate.separation =

# Width of Gaussian blurring kernel, in pixels Default is 1. May be an array,
# see "diameter" for details.
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.locate.html
# (positive float or 2-element array thereof, required, default: 1)
trackpy_locate.noise_size = 1

# The size of the sides of the square kernel used in boxcar (rolling average)
# smoothing, in pixels Default is diameter. May be an array, making the kernel
# rectangular. If not given defaults to the same value as "diameter".
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.locate.html
# (positive float or 2-element array thereof, optional)
# trackpy_locate.smoothing_size =

# Clip bandpass result below this value. Thresholding is done on the already
# filtered and smoothed image. If not provided defaults to 1 for integer images
# and 1/255 for float images.
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.locate.html
# (float, optional)
# trackpy_locate.threshold =

# Features must have a peak brighter than pixels in this percentile. This helps
# eliminate spurious peaks.
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.locate.html
# (positive float in [0, 100], required, default: 64)
trackpy_locate.percentile = 64

# Return only the N brightest features above minmass. If not given, return all
# features above minmass.
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.locate.html
# (positive integer, optional)
# trackpy_locate.topn =

# Toggle preprocessing with bandpass.
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.locate.html
# (boolean, required, default: true)
trackpy_locate.preprocess = true

# Maximal number of loops to refine the center of mass.
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.locate.html
# (positive integer, required, default: 10)
trackpy_locate.max_iterations = 10

# Compute additional particle characteristics: eccentricity, signal, ep.
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.locate.html
# (boolean, required, default: true)
trackpy_locate.characterize = true

# Explicitly set whether "numba" is used.
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.locate.html
# ("auto" / "python" / "numba", required, default: "auto")
trackpy_locate.engine = "auto"

# Specify the number of threads to use to locate frames in parallel.
# (integer > 0, required, default: 4)
parallel = 4

# Minimal euclidean distance for two particles to be considered a pair.
# (positive float, required, default: 0)
helix.min_distance = 0

# Maximal euclidean distance for two particles to be considered a pair.
# (positive float, required, default: 20)
helix.max_distance = 20

# If True, the old positions of the two particles forming a pair are stored
# inside the columns "x_old_i" and "y_old_i" with i in [1, 2].
# (boolean, required, default: false)
helix.save_old_pos = false

# Controls how the ambiguity is handled if a particle has more than one
# candidate to form a pair with. The following strategies are possible:
# "combination" - Pairs are formed from all possible combinations.
# "closest" - The closest candidate by euclidean distance is chosen.
# "drop" - All ambiguous particles are removed.
# ("combination" / "closest" / "drop", required, default: "combination")
helix.ambiguity_handling = "combination"


[step_link]
# This section contains the configuration for the linking step of the "process"
# command.

# The maximum distance features can move between frames, optionally per
# dimension.
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.link.html
# (positive integer, required, default: 10)
trackpy_link.search_range = 10

# The maximum number of frames during which a feature can vanish, then
# reappear nearby, and be considered the same particle. 0 by default.
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.link.html
# (positve integer, required, default: 0)
trackpy_link.memory = 0

# If given, when encountering an oversize subnet, retry by progressively
# reducing the "search_range" (see above) until the subnet is solvable. If
# "search_range" becomes <= "adaptive_stop", give up and terminate.
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.link.html
# (positive float, optional)
# trackpy_link.adaptive_stop =

# Reduce "search_range" by multiplying it by this factor. Only effective if
# "adaptive_stop" (above) is given.
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.link.html
# (float in range (0, 1), required, default: 0.95)
trackpy_link.adaptive_step = 0.95

# Algorithm used to identify nearby features.
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.link.html
# ("KDTree" / "BTree", required, default: "KDTree")
trackpy_link.neighbor_strategy = "KDTree"

# Algorithm used to resolve subnetworks of nearby particles "auto" uses hybrid
# ("numba"+"recursive") if available "drop" causes particles in subnetworks to
# go unlinked.
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.link.html
# ("recursive" / "nonrecursive" / "numba" / "hybrid" / "drop" / "auto",
#  required, default: "auto")
trackpy_link.link_strategy = "auto"

# Minimum number of points (video frames) to survive.
# http://soft-matter.github.io/trackpy/v0.4.1/generated/trackpy.filtering.filter_stubs.html
# (positive integer, required, default: 10)
trackpy_filter_stubs.threshold = 10


[step_diff]

# Calculate displacement between frames that are this many steps apart.
# (positive integer, required, default: 1)
diff_step = 1
